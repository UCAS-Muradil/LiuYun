package LiuYun

import spinal.core._
import spinal.lib._

class CoreDebug extends Bundle{
    val pc      :UInt = LISA.GPR
    val rf_wen  :UInt = UInt(4 bits)
    val rf_wnum :UInt = UInt(5 bits)
    val rf_wdata:UInt = LISA.GPR
}
