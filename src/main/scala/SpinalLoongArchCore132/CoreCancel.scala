package LiuYun

import spinal.core._
import spinal.lib._

class CoreCancel extends Bundle{
    val cancel:Bool = Bool()
    val target:UInt = LISA.GPR
    val ghr:Bits = Bits(BTBConfig.Ghrwidth bits)
    def @=:(pc:UInt):Unit = {
        when(cancel){
            pc := target
        }
    }
    def @==:(x:Bits):Unit = {
        when(cancel){
            x := ghr
        }
    }
}
class BrBus extends CoreCancel{
    val idle:Bool = Bool()
}

class CoreControl() extends Bundle with IMasterSlave
{
  val ex = new CoreCancel()
  val br = new BrBus()
  override def asMaster():Unit = out(this)
}