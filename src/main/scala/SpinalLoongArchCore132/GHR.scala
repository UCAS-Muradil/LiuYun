package LiuYun

import spinal.core._
import spinal.lib._

object THR{
  var shift  = 2
  var rotate = 1
  def update(ghr:Bits,pc:UInt,target:UInt):Bits = {
    val w = ghr.getWidth
    (ghr |<< shift) ^ (pc>>2).asBits.resize(w) ^ (target>>2).asBits.resize(w).rotateLeft(rotate)
  }

  def xor(pc:UInt, ghr:Bits, width:Int):UInt = {
    val src = (pc >> 2).asBits
    (src.takeLow(width) ^ ghr.takeLow(width) ^ (src >> width).takeLow(width)).asUInt
  }

  def xor(pc:UInt, width:Int):UInt = {
    val src = (pc >> 2).asBits
    (src.takeLow(width) ^ (src >> width).takeLow(width)).asUInt
  }
}