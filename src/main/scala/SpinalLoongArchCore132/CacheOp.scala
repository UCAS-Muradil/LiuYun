package LiuYun

import spinal.core._
import spinal.lib._

trait HasCacopCode{
    val code:UInt
    def isICache = code(2 downto 0) === U(0)
    def isDCache = code(2 downto 0) === U(1)
    def isSCache = code(2 downto 0) === U(2)
    def isSt0Tag = code(4 downto 3) === U(0)
    def isIdxInv = code(4 downto 3) === U(1)
    def isHitInv = code(4 downto 3) === U(2)
    class DecodedCacop extends Bundle{
        val st0tag = Bool()
        val idxinv = Bool()
        val hitinv = Bool()
    }
    def getEnables(valid:Bool) = {
        val op = new DecodedCacop
        op.st0tag := valid && isSt0Tag
        op.idxinv := valid && isIdxInv
        op.hitinv := valid && isHitInv
    }
}
class Cacop extends Bundle with HasCacopCode{
    val valid = Bool()
    val code  = UInt(5 bits)
}
class CacopInfo extends Bundle with HasCacopCode{
    val code  = UInt(5 bits)
    val ptag  = UInt(20 bits)
    val vidx  = UInt(12 bits)
    def pa = ptag @@ vidx
}
class CacopBus extends CacopInfo with IMasterSlave{
    val valid = Bool()
    val ready = Bool()
    override def asMaster():Unit = {
        out(valid,code,ptag,vidx)
        in (ready)
    }
}
