package LiuYun

import spinal.core._
import spinal.lib._

object GenerateVerilog{
    def main(args: Array[String]): Unit = {
        val config = SpinalConfig(mode = Verilog
            , removePruned = true
            , oneFilePerComponent=true
            , targetDirectory="./gen/rtl")
        if(args.length == 1){
            val report = args(0) match {
                case "StageFe1" => config.generate(new StageFe1)
                case "StageFe2" => config.generate(new StageFe2)
                case "StageDe"  => config.generate(new StageDe)
                case "StageEx1" => config.generate(new StageEx1)
                case "StageEx2" => config.generate(new StageEx2)
                case "StageEx3" => config.generate(new StageEx3)
                case "StageWb"  => config.generate(new StageWb)
                case "CsrFile"  => config.generate(new CsrFile)
                //case "Tlb"      => config.generate(new Tlb)
                case "RegFile"  => config.generate(new RegFile(2))
                case "Instructions"  => 
                    Instructions.dumpSubOpCodes
                    config.generate(new StageDe)
            }
        }
        else if(args.length == 0){
            config.generate(new CoreTop)
        }
    }
}
