package LiuYun

import spinal.core._
import spinal.lib._

class StageFe2 extends Component{
    val io = new Bundle{
        val ic      :PipeCtrl = slave (new PipeCtrl)
        val idat    :SDatFe2  = in    (new SDatFe2 )
        val oc      :PipeCtrl = master(new PipeCtrl)
        val odat    :SDatDe   = out   (new SDatDe  )
        val iresp   :InstResp = slave (new InstResp)
        val iexcpt  :IExcept  = in    (new IExcept )
        val brbus   :BrBus      = in  (new BrBus)
        val exbus   :CoreCancel = in  (new CoreCancel)
    }
    val cancel:Bool = io.brbus.cancel || io.exbus.cancel;
    val data:SDatDe  = new SDatDe assignedFrom io.idat
    val pipe:DoubledStageBuffer[SDatDe] = new DoubledStageBuffer[SDatDe](data)
    pipe.update(io.ic,io.oc,cancel)
    io.odat := pipe.data
    pipe.newex := io.iexcpt.ex
    when(!io.ic.ex){
        when(io.iexcpt.ex){
            data.ecode := io.iexcpt.ecode
        }
    }
    data.inst := io.iresp.inst
    pipe.ready := io.iresp.valid && io.ic.valid || io.ic.ex
    io.iresp.allow := !pipe.valid.last
    io.iresp.cancel := cancel
}
