package LiuYun

import spinal.core._
import spinal.lib._

object LISA{
    val ISA = "LoongArch32 Reduced"
    var withMMU:Boolean = true
    val w_tlbidx:Int = L2TLB.WAY_BIT + L2TLB.SET_BIT
    val w_gpr:Int = 32
    val palen:Int = 32
    def w_ppn:Int = palen - 12
    val w_intr :Int = 8
    val w_asid :Int = 10
    val psizes:Array[Int] = Array(12,21)
    val ps_min:Int = LISA.psizes.min
    val ps_max:Int = LISA.psizes.max
    val ps_n:Int = psizes.length
    val tlbrd_style = "clear"
    def hasDmwPriority = false
    def VADDR_WIDTH:Int = VALEN
    def PADDR_WIDTH:Int = palen
    def TAG_WIDTH:Int = w_ppn
    def VALEN:Int = 32
    def BASIC_PS = ps_min
    def HUGE_PS = ps_max
    def TAG_LO = VADDR_WIDTH - TAG_WIDTH
    def ASID_WIDTH = w_asid
    def TLB_IDX_BIT = w_tlbidx

    if(tlbrd_style != "clear"){
        // for "keep", tlbrd do not modify csr fields like vppn when not exists
        println(s"""style of TLBRD miss "${tlbrd_style}" is deprecated, please use "clear".""")
    }
    if(hasDmwPriority){
        println(s"""Using DMW with priority, which is deprecated.""")
    }

    def bootPC :UInt = U"h1C000000"
    def GPR = UInt(w_gpr bits)
    def PA  = UInt(palen bits)

    trait ExFactory{
        def code:UInt
        def subc:UInt
    }
    class Ex(val v_code:Int,val v_subc:Int) extends ExFactory{
        def code:UInt = Ex.Code(v_code)
        def subc:UInt = Ex.Subc(v_subc)
    }
    case class CondEx(cond:Bool,whenT:Ex,whenF:Ex) extends ExFactory{
        override def code:UInt = Mux(cond,whenT.code,whenF.code)
        override def subc:UInt = Mux(cond,whenT.subc,whenF.subc)
    }
    object Ex{
        val w_code:Int = 6
        val w_subc:Int = 9
        def Code(id:Int):UInt = U(id, w_code bits)
        def Subc(id:Int):UInt = U(id, w_subc bits)
        def Code:UInt = UInt(w_code bits)
        def Subc:UInt = UInt(w_subc bits)
        def apply(code:Int,subc:Int=0):Ex = new Ex(code,subc)
        val INT :Ex = Ex(0x0)
        val PIL :Ex = Ex(0x1)
        val PIS :Ex = Ex(0x2)
        val PIF :Ex = Ex(0x3)
        val PME :Ex = Ex(0x4)
        val PPI :Ex = Ex(0x7)
        val ADE :Ex = Ex(0x8)
        val ADEF:Ex = Ex(0x8,0x0)
        val ADEM:Ex = Ex(0x8,0x1)
        val ALE :Ex = Ex(0x9)
        val SYS :Ex = Ex(0xb)
        val BRK :Ex = Ex(0xc)
        val INE :Ex = Ex(0xd)
        val IPE :Ex = Ex(0xe)
        val FPD :Ex = Ex(0xf)
        val FPE :Ex = Ex(0x12,0x0)
        val TLBR:Ex = Ex(0x3f)
        def PISL(st:Bool) = CondEx(st,PIS,PIL)
        def isMem(code:UInt):Bool = code.orR && code <= ALE.code
        def isTlb(code:UInt):Bool = code.orR && code <= PPI.code
    }

    object TLB{
        def FETCH:Int = 0
        def DATA:Int  = 1
        def SRCHOP:Int = 2
        def TYPE_WIDTH:Int = log2Up(SRCHOP+1)

        def VPPN_LEN:Int = VALEN - ps_min - 1
        def VPPN_LEN_HUGE:Int = VALEN - ps_max - 1
        def VPPN_LEN_BASIC:Int = VPPN_LEN
        def PPN_WIDTH:Int = palen - BASIC_PS
    }

    object L1TLB{
        def CAP:Int = 8
        def BIT:Int = log2Up(CAP)
    }

    object L2TLB{
        def WAY_NUM:Int = 16
        def WAY_BIT = log2Up(WAY_NUM)
        def SET_NUM:Int = 1
        def SET_BIT = log2Up(SET_NUM)
        def TAG_LEN_BASIC = TLB.VPPN_LEN - SET_BIT
        def TAG_LEN_HUGE  = TLB.VPPN_LEN_HUGE - SET_BIT
        def TAG_LEN       = TAG_LEN_BASIC
    }
}
