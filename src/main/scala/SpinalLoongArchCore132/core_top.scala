package LiuYun

import spinal.core._
import spinal.lib._

import spinal.lib.bus.amba4.axi._

class core_top extends Component{
    val cur_clk:Bool = ClockDomain.readClockWire
    val cur_rst:Bool = ClockDomain.readResetWire
    val intrpt :UInt = in(UInt( 8 bits))
    cur_clk.setName("aclk")
    cur_rst.setName("aresetn")
    val axi = master(new LabAxi3)
    val debug0_wb  = out(new CoreDebug)
    axi.renamed("axi_")
    val core = new CoreTop
    core.io.core.intr := intrpt
    core.io.core.id   := U(0)
    core.io.core.ipi  := False
    axi <> core.io.axi
    debug0_wb := core.io.debug_wb
}

object GenerateVerilogForChiplab{
    def main(args: Array[String]): Unit = {
        val clk_dom = ClockDomainConfig(resetKind=SYNC,resetActiveLevel=LOW)
        val config = SpinalConfig(mode = Verilog
            , removePruned = false
            , oneFilePerComponent=true
            , defaultConfigForClockDomains = clk_dom
            , targetDirectory="./gen/rtl")
        config.generate(new core_top)
    }
}
