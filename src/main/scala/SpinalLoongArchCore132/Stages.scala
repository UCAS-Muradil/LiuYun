package LiuYun

import spinal.core._
import spinal.lib._

class SDatFe1 extends Bundle{
    val pc:UInt = LISA.GPR
    def this(pc:UInt){
        this()
        this.pc := pc
    }
}
class SDatFe2 extends Bundle{
    val pc   :UInt = LISA.GPR
    val ecode:UInt = LISA.Ex.Code
    val predtarget :UInt = LISA.GPR
    val ghr = Bits(BTBConfig.Ghrwidth bits)
    val btbsave  = new BTBSave
    def assignedFrom(prev:SDatFe1):this.type = {
        pc := prev.pc
        this
    }
}
class SDatDe extends Bundle{
    val pc   :UInt = LISA.GPR
    val ecode:UInt = LISA.Ex.Code
    val inst :UInt = UInt(32 bits)
    val predtarget :UInt = LISA.GPR
    val ghr = Bits(BTBConfig.Ghrwidth bits)
    val btbsave  = new BTBSave
    def assignedFrom(prev:SDatFe2):this.type = {
        pc    := prev.pc
        ecode := prev.ecode
        predtarget := prev.predtarget
        ghr := prev.ghr
        btbsave  := prev.btbsave
        this
    }
}
class SDatEx extends Bundle{
    val pc   :UInt = LISA.GPR
    val ecode:UInt = LISA.Ex.Code
    val inst :UInt = UInt(32 bits)
    val dest :UInt = UInt( 5 bits)
    val grwr :Bool = Bool()
    val itype:InstTypeInfo = new InstTypeInfo
    val subcode:UInt = Instructions.OpSubCode
    val ghr = Bits(BTBConfig.Ghrwidth bits)
}
class SDatEx1 extends SDatEx{
    val a    :UInt = LISA.GPR
    val b    :UInt = LISA.GPR
    val predtarget :UInt = LISA.GPR
    val btbsave  = new BTBSave
    def assignedFrom(prev:SDatDe):this.type = {
        pc    := prev.pc
        ecode := prev.ecode
        inst  := prev.inst
        predtarget := prev.predtarget
        ghr := prev.ghr
        btbsave  := prev.btbsave
        this
    }
}
class SDatEx2 extends SDatEx{
    val value:UInt = LISA.GPR
    val mul_x:UInt = UInt(64 bits)
    val mul_y:UInt = UInt(64 bits)
    val va   :UInt = LISA.GPR
    val tv :UInt = UInt(64 bits)
    val wdata :UInt = LISA.GPR
    val difftest_load :Bits = Bits(5 bits)
    val difftest_store :Bits = Bits(3 bits)
    val tlbfill_index = UInt(LISA.w_tlbidx.bits) //difftest
    val csr_estat:UInt = LISA.GPR
    def assignedFrom(prev:SDatEx1):this.type = {
        this.assignSomeByName(prev)
        this
    }
}
class SDatEx3 extends SDatEx{
    val esubc:UInt = LISA.Ex.Subc
    val value:UInt = LISA.GPR
    val va   :UInt = LISA.GPR
    val tv :UInt = UInt(64 bits)
    val wdata :UInt = LISA.GPR
    val pa  :UInt = LISA.PA //difftest need
    val difftest_load :Bits = Bits(5 bits)
    val difftest_store :Bits = Bits(3 bits)
    val tlbfill_index = UInt(LISA.w_tlbidx.bits) //difftest
    val csr_estat:UInt = LISA.GPR
    def assignedFrom(prev:SDatEx2):this.type = {
        this.assignSomeByName(prev)
        this
    }
}
class SDatWb extends SDatEx{
    val esubc:UInt = LISA.Ex.Subc
    val value:UInt = LISA.GPR
    val va   :UInt = LISA.GPR
    val tv :UInt = UInt(64 bits)
    val wdata :UInt = LISA.GPR
    val pa  :UInt = LISA.PA //difftest need
    val difftest_load :Bits = Bits(5 bits)
    val difftest_store :Bits = Bits(3 bits)
    val tlbfill_index = UInt(LISA.w_tlbidx.bits) //difftest
    val csr_estat:UInt = LISA.GPR
    def assignedFrom(prev:SDatEx3):this.type = {
        this.assignSomeByName(prev)
        this
    }
}
