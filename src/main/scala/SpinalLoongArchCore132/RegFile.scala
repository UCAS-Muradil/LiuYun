package LiuYun

import spinal.core._
import spinal.lib._

class RFRead extends Bundle with IMasterSlave{
    val addr = UInt(5 bits)
    val data = LISA.GPR
    def read(rf:Mem[UInt]){
        data := Mux(addr =/= 0,rf.readAsync(addr),U(0))
    }
    override def asMaster():Unit = {
        out(addr)
        in(data)
    }
}
class RFWrite extends Bundle{
    val en   = Bool()
    val addr = UInt(5 bits)
    val data = LISA.GPR
    def write(rf:Mem[UInt]){
        rf.write(
            enable  = en,
            address = addr,
            data    = data
        )
    }
}
class RegFile(nports:Int) extends Component{
    val io = new Bundle{
        val r = Vec(slave(new RFRead),nports)
        val w = in(new RFWrite)
        val difftest = out(new DiffGreg)
    }
    val rf = Mem(LISA.GPR,32)
    for(port <- io.r){
        port.read(rf)
    }
    io.w.write(rf)

    io.difftest.coreid := B(0, 8 bits)
    io.difftest.gpr(0) := U(0,32 bits)
    for(i <- 1 to 31){
        io.difftest.gpr(i) := rf.readAsync(U(i,5 bits))
    }
}
