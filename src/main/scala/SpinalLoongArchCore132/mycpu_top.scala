package LiuYun

import spinal.core._
import spinal.lib._

import spinal.lib.bus.amba4.axi._

class mycpu_top extends Component{
    val cur_clk:Bool = ClockDomain.readClockWire
    val cur_rst:Bool = ClockDomain.readResetWire
    cur_clk.setName("aclk")
    cur_rst.setName("aresetn")
    val axi = master(new LabAxi3)
    val debug_wb  = out(new CoreDebug)
    axi.renamed("axi_")
    val core = new CoreTop
    core.io.core.setEmpty
    axi <> core.io.axi
    debug_wb := core.io.debug_wb
}

object GenerateVerilogForMyCPU{
    def main(args: Array[String]): Unit = {
        RamConfig.use_vivado = true
        DifftestConfig.use_difftest = false
        val clk_dom = ClockDomainConfig(resetKind=SYNC,resetActiveLevel=LOW)
        val config = SpinalConfig(mode = Verilog
            , removePruned = true
            , oneFilePerComponent=true
            , defaultConfigForClockDomains = clk_dom
            , targetDirectory="./gen/rtl")
        config.generate(new core_top)
    }
}
