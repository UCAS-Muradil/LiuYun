# 流云核设计介绍
“行知”系列“流云”处理器是是一款基于LoongArch32 Reduced指令集的单发射7级流水通用中央处理器核，对外实现AXI总线通信

本处理器追求高FPGA综合主频和低面积低功耗，由Spinal HDL提供了高效的描述方式，既是对简单流水线CPU的探索和回顾，也是对探索方式的尝试

## 流水结构说明

### 流水级

#### FE1
&emsp;计算PC,并发出取指请求

#### FE2
&emsp;等待指令返回，发给下一级

#### DE
&emsp;译码级，同时**访问通用寄存器**。将指令相关控制信号和所需的寄存器值传给下一级

#### EX1
&emsp;根据指令行为，以及**读取的CSR相关信息**，进行ALU运算  
&emsp;若指令为访存指令，会在该流水级**发送访存请求**  
&emsp;若指令为跳转指令且跳转预测错误，将在该流水级报出并**回滚**   
&emsp;若指令会改变CSR寄存器的值，将在该流水极等待后续指令执行完后发送**写CSR请求**并**回滚**

&emsp;写csr信号：


    val we  :Bool = mayw && io.ic.empty

&emsp;回滚信号（后面的指令从取指开始重新执行）：


    val roll:Bool = io.ic.empty && (csr.mayw || tlb.mayw || idle) || (shouldNotTaken && io.ic.allow)
    when(roll){
        io.brbus.cancel := True
        io.brbus.target := seqpc
    }

#### EX2
&emsp;若有访问tlb的需求，会在得到tlb结果后再流水  
&emsp;乘法指令第二阶段将在该流水级继续计算并算出结果

#### EX3
&emsp;等待访存或除法指令结果，将结果传给下一级

#### WB
&emsp;检查是否有例外，若有，报例外；若无，**写通用寄存器**  
&emsp;若发生例外，ecode和subcode将在该流水级写入

### 流水间关键控制信号说明
&emsp;allow信号：发给上一级，表示已准备好接收上一级的流水信息  
&emsp;empty信号：发给上一级，表示当前流水级以及后续流水级指令已提交排空  
&emsp;valid信号：发给下一级，表示发送的流水内容有效，需执行  
&emsp;ex信号：发给下一级，表示有例外（该信号一级一级检查并传递，在WB级处理）  
&emsp;go信号：关键信号，为1时传递流水

    go := ic.valid && ready && maygo

## TLB结构说明  
&emsp;TLB采用二级结构，具体参数可在`LoongArch.scala`内更改配置  
&emsp;二级TLB为组相联，当SET_NUM为1，则变为全相联（chiplab功能测试需满足全相联）  
&emsp;由于LoongArch支持大小页，组相联配置下，根据某种页大小index查询未命中时，需再次查询另一种页大小对应index（即代码中的re-srch）  
&emsp;**注意**：当二级TLB设置较大跑chiplab时，最好关闭trace比对（当表项过多时，NEMU模拟器查询TLB速度较慢），以保证仿真速度不会过慢

## Cache结构说明
&emsp;Icache和Dcache皆为2路组相联，VIPT的16B*256项（4KB）结构

## BTB结构说明
&emsp;目前实现了四类BTB供选择和示例，如需替换在`CoreTop.scala`中的CoreTop内选择实例化不同的BTB  
&emsp;默认BTB的Tag为12 bits（PC【13：2】），可通过`BTB.scala`中的BTBConfig更改配置

### NoneBTB
&emsp;即永远只会预测不跳转的空白BTB

### SimpleBTB
&emsp;存储Tag，Target，Taken（1 bit）信息, 若Tag命中，则根据上一次跳转情况进行预测

### NormalBTB
&emsp;存储Tag, Target, sc(2 bits)信息，sc为2位的饱和计数器。若Tag命中，则根据饱和计数器情况进行预测。饱和计数器的更新会根据指令类型及跳转情况更新  
&emsp;BTB不会保存非跳转指令对应PC的Tag

### ComplexBTB
&emsp;存储Tag, Target, cond(是否为条件跳转指令), ras（是否为间接跳转）；此外，还有RAS和Gshare分支预测器。  
&emsp;&emsp;若Tag命中，会根据cond,ras的情况进行预测：  
&emsp;&emsp;&emsp;若cond=1，预测结果来自于分支预测器;  
&emsp;&emsp;&emsp;若ras=1，预测跳转，且预测跳转地址为RAS栈顶;  
&emsp;&emsp;&emsp;若都不是，则预测跳转（此时应为命中了绝对跳转）

