ThisBuild / version := "1.0"
ThisBuild / scalaVersion := "2.12.16"

val spinalVersion = "1.7.2"
val spinalCore = "com.github.spinalhdl" %% "spinalhdl-core" % spinalVersion
val spinalLib = "com.github.spinalhdl" %% "spinalhdl-lib" % spinalVersion
val spinalIdslPlugin = compilerPlugin("com.github.spinalhdl" %% "spinalhdl-idsl-plugin" % spinalVersion)

val sciss = "de.sciss" %% "sheet" % "0.1.2"

lazy val root = (project in file("."))
  .settings(
    name := "LiuYun",
    libraryDependencies ++= Seq(spinalCore, spinalLib, spinalIdslPlugin, sciss)
  )

fork := true
