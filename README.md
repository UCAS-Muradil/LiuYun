# “流云”处理器

## 介绍

“行知”系列“流云”处理器是是一款基于LoongArch指令集的单发射7级流水通用中央处理器核。

本处理器追求高FPGA综合主频和低面积低功耗，
由Spinal HDL提供了高效的描述方式，
既是对简单流水线CPU的探索和回顾，
也是对探索方式的尝试。

此外，本处理器将来可为多核片上网络提供基本的CPU组件（尚未支持）。  

**更加详细的结构图以及结构说明请看`doc`目录**

## 使用说明

1.  sbt run
2.  选择GenerateVerilogForMyCPU，生成适用于教学实验环境的代码
3.  选择GenerateVerilogForChiplab，生成适用于Chiplab环境的代码
    （流云核需要用到的xilinx定制IP放在了FPGA-IP目录下）
